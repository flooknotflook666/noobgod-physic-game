﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEffect : MonoBehaviour
{
    public GameObject effect;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            effect.SetActive(true);
        }
        
    }
}
    // Start is called before the first frame update
   

