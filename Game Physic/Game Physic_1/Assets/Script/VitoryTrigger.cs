﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VitoryTrigger : MonoBehaviour
{
    public GameObject Victorymenu;
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Victorymenu.SetActive(true);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
