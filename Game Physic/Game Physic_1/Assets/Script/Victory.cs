﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Victory : MonoBehaviour
{
    [SerializeField] Button _restartButton;
    [SerializeField] Button _MenuButton;
    [SerializeField] Button _quitButton;
   
    void Start()
    {
        _restartButton.onClick.AddListener(
  delegate { RestartButtonClick(_restartButton); });
        _MenuButton.onClick.AddListener(
  delegate { MenuButtonClick(_MenuButton); });
        _quitButton.onClick.AddListener(
  delegate { QuitButtonClick(_quitButton); });
    }
    
    public void RestartButtonClick(Button button)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void MenuButtonClick(Button button)
    {
        SceneManager.LoadScene("Scenemanage");
    }
    public void QuitButtonClick(Button button)
    {
        Application.Quit();
    }

}
