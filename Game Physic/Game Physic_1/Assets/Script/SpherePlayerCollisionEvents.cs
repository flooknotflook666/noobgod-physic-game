﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpherePlayerCollisionEvents : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision other)
 {
 if (other.gameObject.tag == "Underground") {
 Rigidbody rb = this.gameObject.GetComponent <Rigidbody >();
 rb.AddForce(new Vector3(0, 2, 0), ForceMode.Impulse);
 } 

 if (other.gameObject.tag == "Trap") {
 Rigidbody rb = this.gameObject.GetComponent <Rigidbody >();
 rb.AddForce(new Vector3(0, 6, 0), ForceMode.Impulse);
 } 

 if (other.gameObject.tag == "Slow") {
 Rigidbody rb = this.gameObject.GetComponent <Rigidbody >();
 rb.AddForce(new Vector3(0, -4, 0), ForceMode.Impulse);
 } 


 if (other.gameObject.tag == "Boost") {
 Rigidbody rb = this.gameObject.GetComponent <Rigidbody >();
 rb.AddForce(new Vector3(0, 0, 4), ForceMode.Impulse);
 } 

  if (other.gameObject.tag == "Drag") {
 Rigidbody rb = this.gameObject.GetComponent <Rigidbody >();
 rb.AddForce(new Vector3(0, -5, -4), ForceMode.Impulse);
 } 
 }


}
